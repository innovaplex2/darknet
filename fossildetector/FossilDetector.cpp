#include "FossilDetectorDarkNet.h"
#include <atlbase.h>
#include <exception>

HANDLE	__stdcall	SetupFossilDetector(const char* szJSONConfig) throw(const char*)
{
	IFossilDetector *pDetector;

	try
	{
		pDetector = new CFossildetectorDarkNet();
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	if (!pDetector->Open(szJSONConfig)){
		AtlTrace("Fail Open FossilDetector \n");
		return NULL;
	}

	return pDetector;
}


void	__stdcall	EndupFossilDetector(HANDLE handle) throw(const char*)
{
	if (handle){
		IFossilDetector *pDetector = (IFossilDetector *)handle;

		try
		{
			pDetector->DestorySelf();
		}
		catch (std::exception& ex)
		{
			throw ex.what();
		}
	}
}


BOOL	__stdcall	ProcessFossilDetector(HANDLE handle, int width, int height, int step, BYTE *pData, int* pMaxDetectCount) throw(const char*)
{
	if (handle == NULL){
		AtlTrace("ProcessFossilDetector not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *)handle;
	BOOL res_proc = FALSE;

	try
	{
		res_proc = pDetector->ProcessFossilDetector(width, height, step, pData, pMaxDetectCount);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	return res_proc;
}


BOOL	__stdcall	GetFossilObject(HANDLE handle, FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount) throw(const char*)
{
	if (handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *)handle;
	BOOL res_proc = FALSE;

	try
	{
		res_proc = pDetector->GetFossilObject(pOjbectInfo, pDetectCount);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	return res_proc;
}


BOOL	__stdcall	UpdateFossilObject(HANDLE handle, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo) throw(const char*)
{
	if (handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *)handle;
	BOOL res_proc = FALSE;

	try
	{
		res_proc = pDetector->UpdateFossilObject(nDetectCount, pOjbectInfo);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	return res_proc;
}


BOOL	__stdcall	ProcessUpdateFossilObject(HANDLE handle, int width, int height, int step, BYTE *pData, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo) throw(const char*)
{
	if (handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *)handle;
	BOOL res_proc = FALSE;

	try
	{
		res_proc = pDetector->ProcessUpdateFossilObject(width, height, step, pData, nDetectCount, pOjbectInfo);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	return res_proc;
}


BOOL	__stdcall	UpdateFossilObject2(HANDLE handle, int nDetectCount, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos) throw(const char*)
{
	if (handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *)handle;
	BOOL res_proc = FALSE;

	try
	{
		memcpy(pOutFossilInfos, pInFossilInfos, sizeof(FOSSILDETECTInfo)*nDetectCount);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	try
	{
		res_proc = pDetector->UpdateFossilObject(nDetectCount, pOutFossilInfos);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	return res_proc;
}


BOOL	__stdcall	ProcessUpdateFossilObject2(HANDLE handle, int width, int height, int step, BYTE *pData,
	int nDetectCount, FOSSILDETECTInfo* pInFossilInfos, FOSSILDETECTInfo* pOutFossilInfos) throw(const char*)
{
	if (handle == NULL){
		AtlTrace("GetFossilObject not handle set \n");
		return FALSE;
	}

	IFossilDetector *pDetector = (IFossilDetector *)handle;
	BOOL res_proc = FALSE;

	try
	{
		memcpy(pOutFossilInfos, pInFossilInfos, sizeof(FOSSILDETECTInfo)*nDetectCount);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	try
	{
		res_proc = pDetector->ProcessUpdateFossilObject(width, height, step, pData, nDetectCount, pOutFossilInfos);
	}
	catch (std::exception& ex)
	{
		throw ex.what();
	}

	return res_proc;
}


IFossilDetector*	__stdcall	CreateFossilDetector() throw(const char*)
{
	return (IFossilDetector*)new CFossildetectorDarkNet();
}
