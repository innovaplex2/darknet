#pragma once
#include "FossilDetector.h"

#include "../src/layer.h"

extern "C" {
#include "../src/box.h"
#include "../src/utils.h"
#include "../src/network.h"
#include "../src/detection_layer.h"
#include "../src/parser.h"
#include "../src/region_layer.h"
	/*
#include "image.h"
#include "layer.h"
#include "data.h"

	*/
	/*
float * network_predict(struct network, float *);
void	set_batch_network(struct network *, int);
struct network parse_network_cfg(char *);
void load_weights(struct network *, char *);
*/
}

#include "opencv/cv.h"

class CFossildetectorDarkNet : public IFossilDetector
{
public:
	CFossildetectorDarkNet();
	~CFossildetectorDarkNet();

	BOOL	Open(const char *szConfigXML);
	BOOL	ProcessFossilDetector(int width, int height, int step, BYTE *pData, int* pDetectCount);
	BOOL	GetFossilObject(FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount);
	BOOL	UpdateFossilObject(int nDetectCount, FOSSILDETECTInfo *pOjbectInfo) { return FALSE; }
	BOOL	ProcessUpdateFossilObject(int width, int height, int step, BYTE *pData, int nDetectCount, FOSSILDETECTInfo *pOjbectInfo){ return FALSE; }

	void	DestorySelf(){ delete this; }

private:
	network		m_net;
	detection_layer m_detection_layer;

	box *		m_boxes;
	float **	m_probs;
		
	int		m_class_count;
	float	m_threshold;

	cv::Mat		m_input;
	cv::Mat		m_gray_input;
	cv::Scalar	m_avr;
	cv::Scalar	m_var;
	int			m_max_width;

	int		AdjustFossilPostion(BYTE* pSrc, int width, int height, int pitch, int limit_max_width, int* x, int *y, int *weight_w, int *weight_h);
	
	std::vector<FOSSILDETECTInfo> m_FossilList;
	
};

