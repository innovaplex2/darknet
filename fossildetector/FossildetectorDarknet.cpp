#include "FossildetectorDarkNet.h"
#include "../src/utils.h"
#include <exception>
#include <fstream>
#include "opencv/highgui.h"

using namespace std;

#include "../json/include/json.h"
#pragma comment(lib, "../json/" JSON_LIB_PATH)

#ifdef _DEBUG
//#define SHOW_FOSSIL	1
#endif

CFossildetectorDarkNet::CFossildetectorDarkNet()
{
	m_boxes = NULL;
	m_probs = NULL;

	m_max_width = 160;
	m_threshold = 0.14;
	m_class_count = 2;
}


CFossildetectorDarkNet::~CFossildetectorDarkNet()
{
	if (m_boxes) free(m_boxes);
	if (m_probs){
		for (int i = 0; i < m_detection_layer.side*m_detection_layer.side*m_detection_layer.n; i++) free(m_probs[i]);
		free(m_probs);
	}

	free_network(m_net);
}


BOOL	CFossildetectorDarkNet::Open(const char *szConfigXML)
{
	Json::Value DetectorConfig;
	Json::Reader reader;

	std::ifstream inFile(szConfigXML);
	if (!inFile.is_open()){
		return TRUE;
	}

	bool parsedSuccess = reader.parse(inFile, DetectorConfig, false);
	if (parsedSuccess == false) {
		return TRUE;
	}

	std::string		prototxt;
	std::string		train_model;

	int gpu_index = DetectorConfig["gpu_index"].asInt();
	m_max_width = (DetectorConfig["max_width"].asInt() * 3)/2;
	prototxt	= DetectorConfig["darknet_prototxt"].asString();
	train_model = DetectorConfig["darknet_train"].asString();
	m_threshold = DetectorConfig["threshold"].asFloat();
	m_class_count = DetectorConfig["class_count"].asFloat();
	
	int result = TRUE;

	cuda_set_device(gpu_index);

	try{

		m_net = parse_network_cfg((char *)prototxt.c_str());
		load_weights(&m_net, (char *)train_model.c_str());
		
		m_detection_layer = m_net.layers[m_net.n - 1];
		set_batch_network(&m_net, 1);
		srand(2222222);

		m_boxes = (box *)calloc(m_detection_layer.w*m_detection_layer.h*m_detection_layer.n, sizeof(box));
		m_probs = (float **)calloc(m_detection_layer.w*m_detection_layer.h*m_detection_layer.n, sizeof(float *));
		for (int j = 0; j < m_detection_layer.w*m_detection_layer.h*m_detection_layer.n; ++j) 
			m_probs[j] = (float *)calloc(m_detection_layer.classes, sizeof(float *));
	}
	catch (exception& e){
		result = FALSE;
	}

	return result;
}


void convert_yolo_detections(float *predictions, int classes, int num, int square, int side, int w, int h, float thresh, float **probs, box *boxes, int only_objectness)
{
	int i, j, n;
	//int per_cell = 5*num+classes;
	for (i = 0; i < side*side; ++i){
		int row = i / side;
		int col = i % side;
		for (n = 0; n < num; ++n){
			int index = i*num + n;
			int p_index = side*side*classes + i*num + n;
			float scale = predictions[p_index];
			int box_index = side*side*(classes + num) + (i*num + n) * 4;
			boxes[index].x = (predictions[box_index + 0] + col) / side * w;
			boxes[index].y = (predictions[box_index + 1] + row) / side * h;
			boxes[index].w = pow(predictions[box_index + 2], (square ? 2 : 1)) * w;
			boxes[index].h = pow(predictions[box_index + 3], (square ? 2 : 1)) * h;
			for (j = 0; j < classes; ++j){
				int class_index = i*classes;
				float prob = scale*predictions[class_index + j];
				probs[index][j] = (prob > thresh) ? prob : 0;
			}
			if (only_objectness){
				probs[index][0] = scale;
			}
		}
	}
}

image convertMat2Image(cv::Mat* src)
{
	unsigned char *data = (unsigned char *)src->data;
	int h = src->rows;
	int w = src->cols;
	int c = src->channels();
	int step = src->step[0];

	image out = make_image(w, h, c);
	int i, j, k, count = 0;;

	for (k = 0; k < c; ++k){
		for (i = 0; i < h; ++i){
			for (j = 0; j < w; ++j){
				out.data[count++] = data[i*step + j*c + (2-k)] / 255.;
			}
		}
	}
	return out;
}


extern "C" image resize_image2(image im, int w, int h)
{
	image out = make_image(w, h, 3);
	
	for (int c = 0; c < 3; c++){
		float *pInput = im.data + (c*im.w*im.h);
		float *pOutput = out.data + (c*w*h);

		cv::Mat input(im.h, im.w, CV_32FC1, pInput, im.w * 4);
		cv::Mat target(h, w, CV_32FC1, pOutput, w * 4);

		cv::resize(input, target, cv::Size(w, h));
	}
	
	return out;
}



BOOL	CFossildetectorDarkNet::ProcessFossilDetector(int width, int height, int step, BYTE *pData, int* pDetectCount)
{
	if (m_net.n  == 0) return FALSE;

	m_FossilList.clear();

	/*
	IplImage input;
	input.height = height;
	input.width = width;
	input.widthStep = step;
	input.nChannels = 3;
	input.imageData = (char *)pData;
	image out = ipl_to_image(&input);
	//rgbgr_image(out);
	image sized = resize_image2(out, 448, 448);
	*/

	//image sized = resize_image2(width, height, step, pData, m_net.w, m_net.h);
	m_input = cv::Mat(height, width, CV_8UC3, pData);
	cvtColor(m_input, m_gray_input, CV_BGR2GRAY);
	
	cv::Mat target(m_net.h, m_net.w, CV_8UC3);
		
	cv::resize(m_input, target, cv::Size(m_net.w, m_net.h));

	layer l = m_net.layers[m_net.n - 1];

	cv::meanStdDev(m_gray_input, m_avr, m_var);

	image sized = convertMat2Image(&target);
	
	float *X = sized.data;

	float hier_thresh = 0.5f;
	float nms = .5;
	int total = m_detection_layer.h*m_detection_layer.w*m_detection_layer.n;
	float *predictions = network_predict(m_net, X);
	//convert_yolo_detections(predictions, m_detection_layer.classes, m_detection_layer.n, m_detection_layer.sqrt, m_detection_layer.side, 1, 1, m_threshold, m_probs, m_boxes, 0);
	get_region_boxes(l, 1, 1, m_threshold, m_probs, m_boxes, 0, 0, hier_thresh);
	if (nms) do_nms_sort(m_boxes, m_probs, total, m_detection_layer.classes, nms);

	//draw_detections(im, l.side*l.side*l.n, thresh, boxes, probs, voc_names, voc_labels, 20);
	int count = 0;
	for (int i = 0; i < total; i++){
		int class_num = max_index(m_probs[i], m_class_count);
		float prob = m_probs[i][class_num];

		if (prob > m_threshold){
			box b = m_boxes[i];

			int left = (b.x - b.w / 2.)*width;
			int right = (b.x + b.w / 2.)*width;
			int top = (b.y - b.h / 2.)*height;
			int bot = (b.y + b.h / 2.)*height;

			if (left < 0) left = 0;
			if (right > width - 1) right = width - 1;
			if (top < 0) top = 0;
			if (bot > height - 1) bot = height - 1;

			//가장 자리 조건 
			if ((b.x < 0.02) || (b.x > 0.98) || (b.y < 0.02) || (b.y > 0.98)) {
			//if ((b.w > b.x) || (b.h > b.y) || ((1 - b.x) < b.w) || ((1 - b.y) <b.h) )   {
				float ratio = (b.w > b.h) ? (b.w / b.h) : (b.h / b.w);
				if (ratio > 2){
					continue;
				}
			}
			
			
			FOSSILDETECTInfo fossil_info;
			memset(&fossil_info, 0, sizeof(FOSSILDETECTInfo));

			fossil_info.class_no = class_num;
			fossil_info.rc.left = left;
			fossil_info.rc.top = top;
			fossil_info.rc.right = right;
			fossil_info.rc.bottom = bot;

			fossil_info.flag = FOSSIL_UPDATE_PROC_CNN;
			fossil_info.top_idx[0] = class_num;
			fossil_info.top_score[0] = prob * 100;
			fossil_info.fossil_id = count;
					

			m_FossilList.push_back(fossil_info);
			count++;
		}
	}

	*pDetectCount = count;
	return TRUE;
}


BOOL	CFossildetectorDarkNet::GetFossilObject(FOSSILDETECTInfo *pOjbectInfo, int* pDetectCount)
{
	cv::Mat fossil_mask;
	cv::Mat fossil_mask_edge;
		
	int k = 0;
	for (int i = 0; i < m_FossilList.size(); i++){

		//pOjbectInfo[i] = m_FossilList[i];
		cv::Rect rc(m_FossilList[i].rc.left, m_FossilList[i].rc.top, 
			m_FossilList[i].rc.right - m_FossilList[i].rc.left, m_FossilList[i].rc.bottom - m_FossilList[i].rc.top);

		fossil_mask = m_gray_input(rc);
		//cv::Canny(fossil_mask, fossil_mask_edge, m_avr[0] - 10, m_avr[10]+10);
		cv::adaptiveThreshold(fossil_mask, fossil_mask_edge, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C, cv::THRESH_BINARY_INV, 5, 2);
		
		int center_x, center_y;
		int move_x, move_y, increase_x, increase_y;
		int weight_w, weight_h, weight;

		if (AdjustFossilPostion(fossil_mask_edge.data, fossil_mask_edge.cols, fossil_mask_edge.rows, fossil_mask_edge.step.buf[0],
			m_max_width, &center_x, &center_y, &weight_w, &weight_h) < 0){
			//cv::waitKey(0);
			continue;
		}

		move_x = (rc.width) / 2 - center_x;
		move_y = (rc.height) / 2 - center_y;

		//move position by moment.
		rc.x -= move_x;
		rc.y -= move_y;

		weight = (weight_w> weight_h) ? weight_w : weight_h;
		increase_x = (rc.width*weight_w) / 100;
		increase_y = (rc.height*weight_h) / 100;

		//Check boundary and make retangle to squere 
		pOjbectInfo[k] = m_FossilList[i];
		pOjbectInfo[k].rc.left = (rc.x > increase_x) ? (rc.x - increase_x) : 0;
		pOjbectInfo[k].rc.top = (rc.y > increase_y) ? (rc.y - increase_y) : 0;

		pOjbectInfo[k].rc.right = rc.x + rc.width + increase_x;
		pOjbectInfo[k].rc.bottom = rc.y + rc.height + increase_y;

		pOjbectInfo[k].rc.right = (pOjbectInfo[k].rc.right > m_gray_input.cols) ? m_gray_input.cols : pOjbectInfo[k].rc.right;
		pOjbectInfo[k].rc.bottom = (pOjbectInfo[k].rc.bottom > m_gray_input.rows) ? m_gray_input.rows : pOjbectInfo[k].rc.bottom;


#ifdef SHOW_FOSSIL
		rc.x = pOjbectInfo[k].rc.left;
		rc.y = pOjbectInfo[k].rc.top;
		rc.width = pOjbectInfo[k].rc.right - pOjbectInfo[k].rc.left;
		rc.height = pOjbectInfo[k].rc.bottom - pOjbectInfo[k].rc.top;

		cv::Mat fossil_mask2;
		fossil_mask2 = m_gray_input(rc);
		cv::imshow("FOSSILE_INPUT", fossil_mask);
		cv::imshow("FOSSILE_EDGE", fossil_mask_edge);
		cv::imshow("FOSSILE_INPUT2", fossil_mask2);
		cv::waitKey(0);
#endif

		k++;
	}

	*pDetectCount = k;
	return TRUE;
}


#define ABS(x) (((x)<0)?(0-(x)):(x))
int		CFossildetectorDarkNet::AdjustFossilPostion(BYTE* pSrc, int width, int height, int pitch, int limit_max_width, int* x, int *y, int *weight_w, int *weight_h)
{
	int count, sum_x, sum_y, center_x, center_y;
	count = sum_x = sum_y = 0;
	int max_width = (width > limit_max_width) ? limit_max_width : width;

	for(int i = 0 ; i < height ; i++){
		for(int j = 0 ; j < width ; j++){
			if(pSrc[j + i*pitch] == 255){
				count++;
				sum_x += j;
				sum_y += i;
			}
		}
	}
	center_x	= sum_x/count;
	center_y	= sum_y/count;

	sum_x = sum_y = 0;
	float distance= 0;
	int diffx, diify;
	for(int i = 0 ; i < height ; i++){
		for(int j = 0 ; j < width ; j++){
			if(pSrc[j + i*pitch] == 255){
				diffx = ABS(j-center_x);
				diify = ABS(i-center_y);
				sum_x += diffx;
				sum_y += diify;
				distance += (float)sqrt(diffx*diffx + diify*diify);
			}
		}
	}

	distance = distance/count;
	//if( (count < (height*width)/4) && (varinace > sqrt(width*height)/2) ) return -1;
	//반지름의 절반일 경우 (width/sin(45))/2 
	//if( (count < (max_width*max_width)/m_DetectionParam.threadhold_fossil_occapy_ratio) && (distance > max_width/m_DetectionParam.threadhold_fossil_distance_ratio) ) return -1;

	*x	= center_x;
	*y	= center_y ;	//weight to height.... 
	*weight_w	= ((sum_x/count) * 95)/width ;
	*weight_h	= ((sum_y/count) * 95)/height;

	*weight_w	*= max_width/width;
	*weight_h	*= max_width/width;

	return 0;
}



