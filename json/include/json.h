// Copyright 2007-2010 Baptiste Lepilleur
// Distributed under MIT license, or public domain if desired and
// recognized in your jurisdiction.
// See file LICENSE for detail or copy at http://jsoncpp.sourceforge.net/LICENSE

#ifndef JSON_JSON_H_INCLUDED
#define JSON_JSON_H_INCLUDED

#include "autolink.h"
#include "value.h"
#include "reader.h"
#include "writer.h"
#include "features.h"

//Link Static library
#ifdef _M_X64 
	#define JSON_PLATFORM_FLAG	"x64"
#else
	#define JSON_PLATFORM_FLAG	"x86"
#endif

#ifdef _DEBUG
	#define JSON_DEBUG_FLAG	"d"
#else
	#define JSON_DEBUG_FLAG	""
#endif

#ifdef _DLL 
	#define JSON_COMPILE_FLAG	"md"
#else
	#define JSON_COMPILE_FLAG	"mt"
#endif


#define JSON_LIB_PATH	"lib/"JSON_PLATFORM_FLAG"/jsoncpp_"JSON_COMPILE_FLAG""JSON_DEBUG_FLAG".lib" 	


#endif // JSON_JSON_H_INCLUDED
